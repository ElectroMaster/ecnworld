/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author Dell
 */

public class Point2D {
    private int x;
    private int y;
    
    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public Point2D(Point2D p) {
        x = p.getX();
        y = p.getY();
        
    }
    public Point2D() {
        x = 0;
        y = 0;
    }
    
    public void setX(int value) {
        x = value;
    }
    
    public int getX() {
        return x;
    }
    
    public void setY(int value) {
        y = value;
    }
    
    public int getY() {
        return y;
    }
    
    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public void translate(int dx, int dy) {
        x = x + dx;
        y = y + dy;
    }
    
    public Point2D translateP(int dx, int dy) {
        x = x + dx;
        y = y + dy;
        return new Point2D(x, y);
    }
    
    public void affiche() {
        String s;
        s = String.format("[%d; %d]", x, y);
        System.out.println(s);
    }
    
    public float distance(Point2D p) {
        return (float) Math.sqrt(Math.pow((this.getX() - p.getX()),2) + Math.pow((this.getY() - p.getY()),2));
    }
    
    public float distance(int x2, int y2) {
        return (float) Math.sqrt(Math.pow((x2 - x), 2) + Math.pow((y2 - y), 2));
    }
    
}
