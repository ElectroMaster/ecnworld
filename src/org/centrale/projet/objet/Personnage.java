/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.*;

/**
 *
 * @author GUO
 */
public class Personnage extends Creature {

    public String nom;
//    public int ptVie;
//    public int ptPar;
    public int ptMana;
//    public int pourcentageAtt;
//    public int pourcentagePar;
    public int pourcentageMag;
    public int pourcentageResistMag;
//    public int degAtt;
    public int degMag;
    public int distAttMax;
//    Point2D pos;

    public Personnage(String nom, int ptV,
            int ptP, int ptM, int pA, int pP, 
            int pM,int rM, int dA, int dM, 
            int distMax, Point2D pos) {
        this.nom = nom;
        this.ptVie = ptV;
        this.ptPar = ptP;
        this.ptMana = ptM;
        this.pourcentageAtt = pA;
        this.pourcentagePar = pP;
        this.pourcentageMag = pM;
        this.pourcentageResistMag = rM;
        this.degAtt = dA;
        this.degMag = dM;
        this.distAttMax = distMax;
        this.pos = pos;
    }
    
    /**
     * 
     * @param perso un objet et son type est personnage
     */
    public Personnage(Personnage perso) {
        super(perso.ptVie, perso.ptPar, perso.pourcentageAtt, perso.pourcentagePar, perso.degAtt, perso.pos);
        nom = perso.nom;
        ptMana = perso.ptMana;
        pourcentageMag = perso.pourcentageMag;
        pourcentageResistMag = perso.pourcentageResistMag;
        degMag = perso.degMag;
        distAttMax = perso.distAttMax;
    }
    
    /**
     * sans parametres
     */
    public Personnage() {
        Point2D position = new Point2D();
        pos = position;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String value) {
        nom = value;
    }

    public int getPtMana() {
        return ptMana;
    }

    public void setPtMana(int value) {
        ptMana = value;
    }

    public int getPourcentageMag() {
        return pourcentageMag;
    }

    public void setPourcentageMag(int value) {
        pourcentageMag = value;
    }

    public int getPourcentageResistMag() {
        return pourcentageResistMag;
    }

    public void setPourcentageResistMag(int value) {
        pourcentageResistMag = value;
    }

    public int getDegMag() {
        return degMag;
    }

    public void setDegMag(int value) {
        degMag = value;
    }

    public int getDistAttMax() {
        return distAttMax;
    }

    public void setDistAttMax(int value) {
        distAttMax = value;
    }
    
    public void underCure(int value) {
        ptVie = ptVie + value;
    }
    
    public void underMana(int value) {
        ptMana = ptMana + value;
    }
    

    public void affiche() {
        System.out.println("--------------------");
        System.out.println("nom: " + nom);
        System.out.println("ptVie: " + ptVie);
        System.out.println("ptMana: " + ptMana);
        System.out.println("pourcentageAtt: " + pourcentageAtt);
        System.out.println("pourcentagePar: " + pourcentagePar);
        System.out.println("pourcentageMag: " + pourcentageMag);
        System.out.println("pourcentageResistMag: " + pourcentageResistMag);
        System.out.println("degAtt: " + degAtt);
        System.out.println("degMag: " + degMag);
        System.out.println("distAttMax: " + distAttMax);
        System.out.println("pos:[" + pos.getX() + ", " + pos.getY() + "]");

    }

}
