/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.*;

/**
 *
 * @author GUO
 */
public class Monstre extends Creature {



    /**
     * 
     * @param ptV point of life
     * @param ptP point parad
     * @param pA Pourcentage Attack
     * @param pP Pourcentage defence
     * @param dA Attack degree
     * @param pos position of Lapin
     */
    public Monstre(int ptV, int ptP, int pA, int pP, int dA, Point2D pos) {
        super(ptV, ptP, pA, pP, dA, pos);
    }

    /**
     *
     * @param m objet et son type de classe est Monstre
     */
    public Monstre(Monstre m) {
        super(m.getPtVie(), m.getPtPar(),m.getPourcentageAtt(), m.getPourcentagePar(), m.getDegAtt(), m.getPos());
    }

    /**
     * sans parametres
     */
    public Monstre() {
        Point2D position = new Point2D();
        pos = position;
    }


}
