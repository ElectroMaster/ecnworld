/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author GUO
 */
public class Paysan extends Personnage {

    /**
     *
     * @param nom Name of Archer
     * @param ptV Point vie
     * @param ptP Point Parad
     * @param pA Pourcentage Attack
     * @param pP Pourcentage defence
     * @param pM Pourcentage Magique
     * @param rM Pourcentage Resistance of Magic
     * @param dA Attack degree
     * @param dM Magic degree
     * @param distMax Max distance of attack
     * @param pos position of an archer
     */
    public Paysan(String nom, int ptV, int ptP,
            int pA, int pP, int pM,
            int rM, int dA, int dM, int distMax,
            Point2D pos) {
        this.nom = nom;
        this.ptVie = ptV;
        this.ptPar = ptP;
        this.pourcentageAtt = pA;
        this.pourcentagePar = pP;
        this.pourcentageMag = pM;
        this.pourcentageResistMag = rM;
        this.degAtt = dA;
        this.degMag = dM;
        this.distAttMax = distMax;
        this.pos = pos;
    }

    /**
     *
     * @param p objet du type Paysan
     */
    public Paysan(Paysan p) {
        this.nom = p.nom;
        this.ptVie = p.ptVie;
        this.ptPar = p.ptPar;
        this.pourcentageAtt = p.pourcentageAtt;
        this.pourcentagePar = p.pourcentagePar;
        this.pourcentageMag = p.pourcentageMag;
        this.pourcentageResistMag = p.pourcentageResistMag;
        this.degAtt = p.degAtt;
        this.degMag = p.degMag;
        this.distAttMax = p.distAttMax;
        this.pos = p.pos;
    }

    /**
     * sans parametre
     */
    public Paysan() {
        super();

    }

}
