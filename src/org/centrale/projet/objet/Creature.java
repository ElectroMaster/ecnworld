/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.*;

/**
 *
 * @author GUO
 */
public class Creature implements Deplacable{

    public int ptVie;
    public int ptPar;
//    int ptMana;
    public int pourcentageAtt;
    public int pourcentagePar;
//    int pourcentageMag;
//    int pourcentageResistMag;
    public int degAtt;
//    int degMag;
//    int distAttMax;
    public Point2D pos;


    /**
     *
     * @param ptV Point vie
     * @param ptP Point Parad
     * @param pA Pourcentage Attack
     * @param pP Pourcentage defence
     * @param dA Attack degree
     * @param pos position of an archer
     */
    public Creature(int ptV, int ptP, int pA, int pP, int dA, Point2D pos) {
        this.ptVie = ptV;
        this.ptPar = ptP;
        this.pourcentageAtt = pA;
        this.pourcentagePar = pP;
        this.degAtt = dA;
        this.pos = pos;
    }

    /**
     * 
     * @param c objet du type Creature
     */
    public Creature(Creature c) {
        ptVie = c.ptVie;
        ptPar = c.ptPar;
        pourcentageAtt = c.pourcentageAtt;
        pourcentagePar = c.pourcentagePar;
        degAtt = c.degAtt;
        pos = c.pos;
    }

    /**
     * sans parametre
     */
    public Creature() {
        Point2D position = new Point2D();
        pos = position;
    }


    public int getPtVie() {
        return ptVie;
    }

    public void setPtVie(int value) {
        this.ptVie = value;
    }
    
    public int getPtPar(){
        return ptPar;
    }
    
    public void setPtPar(int value){
        this.ptPar = value;
    }

    public int getPourcentageAtt() {
        return pourcentageAtt;
    }

    public void setPourcentageAtt(int value) {
        this.pourcentageAtt = value;
    }
    
    public int getPourcentagePar() {
        return pourcentagePar;
    }

    public void setPourcentagePar(int value) {
        pourcentagePar = value;
    }

    public int getDegAtt() {
        return degAtt;
    }

    public void setDegAtt(int value) {
        this.degAtt = value;
    }

    public Point2D getPos() {
        return pos;
    }

    public void setPos(Point2D value) {
        this.pos = value;
    }

    public void underAttack(int degatSubit) {
        ptVie = ptVie - degatSubit;
    }
    
    /**
     * deplacement aleatoire
     */
    public void randomDeplace() {
        Random generateurAleatoire = new Random();
        int a = generateurAleatoire.nextInt(100);
        int b = generateurAleatoire.nextInt(100);
        pos.translate(a, b);
    }
    

//    public void deplace(ArrayList<Point2D> points) {
//        int nombre = 0;
//        int dx = 0;
//        int dy = 0;
//        System.out.println("Entrer un nombre entre 1,2,3,4,5,6,7,8 pour choisir la direction de deplacement: ");
//        Scanner input = new Scanner(System.in);
//        while (input.hasNext()) {
//            if (input.hasNextInt()) {
//                nombre = input.nextInt();
//                switch (nombre) {
//                    case 1:
//                        dx = -1;
//                        dy = -1;
//                        break;
//                    case 2:
//                        dx = 0;
//                        dy = -1;
//                        break;
//                    case 3:
//                        dx = 1;
//                        dy = -1;
//                        break;
//                    case 4:
//                        dx = -1;
//                        dy = 0;
//                        break;
//                    case 5:
//                        dx = 1;
//                        dy = 0;
//                        break;
//                    case 6:
//                        dx = -1;
//                        dy = 1;
//                        break;
//                    case 7:
//                        dx = 0;
//                        dy = 1;
//                        break;
//                    case 8:
//                        dx = 1;
//                        dy = 1;
//                        break;
//                    default:
//                        System.out.println("Entrez un Integer entre 1 et 8 svp!");
//                        continue;
//                }
//                int i = pos.getX() + dx;
//                int j = pos.getY() + dy;
//                int flag = 0;
//                for (int k = 0; k < points.size(); k++) {
//                    if (i == points.get(k).getX() && j == points.get(k).getY()) {
//                        flag = 1;
//                        break;
//                    }
//                }
//                if (flag == 1) {
//                    System.out.println("Cette case est deja occupee! Veuillez rechoisir un nombre de deplacement:");
//                } else if (flag == 0) {
//                    break;
//                }
//            } else {
//                System.out.println("Entrez un Integer entre 1 et 8 svp!");
//                input.next();
//            }
//        }
//
//        System.out.println("La position initiale est ");
//        pos.affiche();
//        System.out.println("La position apres deplacement dans la direction " + nombre + " est ");
//        pos.translateP(dx, dy).affiche();
//    }
    
    /**
     * 
     * @param points arraylist who save all positions occupied by other creature
     * @param directionIndex direction index from 1 to 8
     * @return bool type, retur true when it deplace successful
     */
    public boolean deplaceSousIndex(ArrayList<Point2D> points, int directionIndex) {
        int dx = 0;
        int dy = 0;

        switch (directionIndex) {
            case 1:
                dx = -1;
                dy = -1;
                break;
            case 2:
                dx = 0;
                dy = -1;
                break;
            case 3:
                dx = 1;
                dy = -1;
                break;
            case 4:
                dx = -1;
                dy = 0;
                break;
            case 5:
                dx = 1;
                dy = 0;
                break;
            case 6:
                dx = -1;
                dy = 1;
                break;
            case 7:
                dx = 0;
                dy = 1;
                break;
            case 8:
                dx = 1;
                dy = 1;
                break;
            default:
                System.out.println("Entrez un Integer entre 1 et 8 svp!");
                return false;
        }
        int i = pos.getX() + dx;
        int j = pos.getY() + dy;
        for (int k = 0; k < points.size(); k++) {
            if (i == points.get(k).getX() && j == points.get(k).getY()) {
                // deja occupe
                System.out.println("Cette case est deja occupee! Veuillez rechoisir un nombre de deplacement:");
                return false;
            }
        }
        pos.translateP(dx, dy);
        return true;
    }
    
    /**
     * 
     * @param dx deviation of x
     * @param dy deviation of y
     * @return type Point2D, position after deplace manner.
     */
    public Point2D deplace(int dx, int dy) {
        pos.translateP(dx, dy);
        return pos;
    }

    public void affiche() {
        System.out.println("--------------------");
        System.out.println("ptVie: " + ptVie);
        System.out.println("pourcentageAtt: " + pourcentageAtt);
        System.out.println("degAtt: " + degAtt);
        String pos_str;
        pos_str = String.format("[%d, %d]", pos.getX(), pos.getY());
        System.out.println("Pos: " + pos_str);
    }
    
}
