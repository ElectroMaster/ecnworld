/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.Random;

/**
 *
 * @author GUO
 */
public class Loup extends Monstre implements Combattant{

    /**
     *
     * @param ptV point of life
     * @param ptP point parad
     * @param pA Pourcentage Attack
     * @param pP Pourcentage defence
     * @param dA Attack degree
     * @param p position of Lapin
     */
    public Loup(int ptV, int ptP, int pA, int pP, int dA, Point2D p) {
        super(ptV, ptP, pA, pP, dA, p);
    }

    /**
     *
     * @param l objet et son type est classe: Lapin
     */
    public Loup(Loup l) {
        ptVie = l.ptVie;
        ptPar = l.ptPar;
        pourcentageAtt = l.pourcentageAtt;
        pourcentagePar = l.pourcentagePar;
        degAtt = l.degAtt;
        pos = l.pos;

    }

    /**
     * sans parametres
     */
    public Loup() {

    }

    /**
     *
     * @param creature objet du type Creature loup ne peut qu' avoir combattre
     * coprs a corps
     */
    public void combattre(Creature creature) {
        int degatSubit = 0;
        if ((int) (this.pos.distance(creature.getPos())) == 1) {
            Random rand = new Random();
            if (rand.nextInt(100) > this.pourcentageAtt) {
                System.out.println("Attaque n'est pas reussi!");
            } else {
                if (rand.nextInt(100) > creature.pourcentagePar) {
                    degatSubit = this.degAtt;
                } else {
                    degatSubit = this.degAtt - creature.ptPar;
                }
            }
        }
        creature.underAttack(degatSubit);
    }

}
