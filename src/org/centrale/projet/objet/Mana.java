/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author GUO
 */
public class Mana extends Potion {
    
    public int baseMana;
    public int soinEffet;
    
    public Mana(String nom, int baseMana) {
        this.nom = nom;
        this.baseMana = baseMana;
        this.potionsize = PotionSize.BASE;
    }
    
    public Mana() {
        
    }
    
    public void setbaseMana(int baseMana) {
        this.baseMana = baseMana;
    }
    
    public int getManaEffet() {
        soinEffet = (int) (baseMana * potionsize.getEffetAmeliore());
        return soinEffet;
    }
    
    public void pickedBy(Creature creature) {
        if(!(creature instanceof Monstre) && (Statut == true)) {
            Personnage man = (Personnage) creature;
            man.underCure(this.getManaEffet());
            Statut = false;
        }
    }
    
    public void affiche(){
        System.out.println("--------------------");
        System.out.println(this.type);
        System.out.println("-------------");
        System.out.println("BaseMana: "+baseMana);
        System.out.println("Potion Size: "+potionsize.name());
        System.out.println("Vrai Mana Effet: "+this.getManaEffet());
    }
    
}
