/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author GUO
 */
public enum PotionSize {
    
    BASE(0), LITTLE(0.2), MEDIUM(0.5), LARGE(1);
    private double data;
    
    private PotionSize(double data) {
        this.data = data;
    }
    public double getEffetAmeliore() {
        return 1 + data;
    }
    
    public void setEffetAmeliore(double data) {
        this.data = data;
    }
    
}
