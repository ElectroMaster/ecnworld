/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author GUO
 */
public class Potion extends Objet {
    
    public String type;
    PotionSize potionsize;
    boolean Statut;
    
    public Potion() {
        nom = "Mysterious Potion";
        potionsize = PotionSize.BASE;
        type = "Potion";
        Statut = true; //potion existe au debut
    }
    
    /**
     * mettre la taille de Potion
     * @param p type num, 4 types: BASE, LITTLE, MEDIUM, LARGE
     */
    public void setPotionSize(PotionSize p) {
        potionsize = p;
        type = "Potion";
    }
    
    /**
     * la potion est pris par quelqu'un
     * @param creature  tous les autres protagonistes
     */
    public void pickedBy(Creature creature) {
        if(!(creature instanceof Monstre) && (Statut == true)) {
            Statut = false;
        }
    }
    
}
