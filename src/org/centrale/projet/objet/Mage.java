/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.Random;

/**
 *
 * @author GUO
 */
public class Mage extends Personnage implements Combattant{
    
    /**
     *
     * @param nom Name of Archer
     * @param ptV Point vie
     * @param ptP Point Parad 
     * @param ptM Point Mana
     * @param pA Pourcentage Attack
     * @param pP Pourcentage defence
     * @param pM Pourcentage Magique
     * @param rM Pourcentage Resistance of Magic
     * @param dA Attack degree
     * @param dM Magic degree
     * @param distMax Max distance of attack
     * @param pos position of an archer
     */
    public Mage(String nom, int ptV, int ptP,
            int ptM, int pA, int pP, int pM, 
            int rM, int dA, int dM, int distMax,
            Point2D pos){
        super(nom, ptV, ptP, ptM, pA, pP, pM, rM, dA, dM, distMax, pos);
    }
    
    /**
     * 
     * @param a objet du type Mage
     */
    public Mage(Mage a){
        super(a.nom, a.ptVie, a.ptMana, a.ptPar, a.pourcentageAtt,
                a.pourcentagePar, a.pourcentageMag, a.pourcentageResistMag,
                a.degAtt, a.degMag, a.distAttMax, a.pos);
    }
    
    /**
     * sans parametre
     */
    public Mage(){
        
    }
    
    public void combattre(Creature creature){
        int degatSubit = 0;
        if((int)(this.pos.distance(creature.pos)) == 1){
            Random rand = new Random();
            if(rand.nextInt(100)>this.pourcentageAtt){
                System.out.println("Attack n'est pas reussi.");

            }
            else{
                if(rand.nextInt(100)>creature.pourcentagePar){
                    degatSubit = this.degAtt;
                }
                else{
                    degatSubit = this.degAtt - creature.ptPar;
                }

            } 
        }
        else if(this.pos.distance(creature.pos) < this.distAttMax){
            Random rand = new Random();
            if(rand.nextInt(100)>this.pourcentageMag){
                System.out.println("Attack n'est pas reussi.");
            }
            else{
                degatSubit = this.degMag;
            } 
        }
        creature.underAttack(degatSubit);
    }
    
}
