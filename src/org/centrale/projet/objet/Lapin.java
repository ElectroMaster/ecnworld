/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author GUO
 */
public class Lapin extends Monstre{
    /**
     * 
     * @param ptV point of life
     * @param ptP point parad
     * @param pA Pourcentage Attack
     * @param pP Pourcentage defence
     * @param dA Attack degree
     * @param p position of Lapin
     */
    public Lapin(int ptV, int ptP, int pA, int pP, int dA, Point2D p){
        super(ptV, ptP, pA, pP, dA, p);
    }
    
    /**
     * 
     * @param l objet et son type est classe: Lapin
     */
    public Lapin(Lapin l){
        ptVie = l.ptVie;
        ptPar = l.ptPar;
        pourcentageAtt = l.pourcentageAtt;
        pourcentagePar = l.pourcentagePar;
        degAtt = l.degAtt;
        pos = l.pos;
    }
    
    /**
     * sans parametres
     */
    public Lapin(){
        
    }
    
}
