/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.ArrayList;

/**
 *
 * @author Exia
 */
public interface Deplacable {
    public Point2D deplace(int dx, int dy);
    public boolean deplaceSousIndex(ArrayList<Point2D> points, int directionIndex);
    public void randomDeplace();
}
