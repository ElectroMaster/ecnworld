/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

/**
 *
 * @author GUO
 */
public class Soin extends Potion {
    
    public int baseSoin;
    public int soinEffet;
    
    public Soin(String nom, int baseSoin) {
        this.nom = nom;
        this.baseSoin = baseSoin;
        this.potionsize = PotionSize.BASE;
    }
    
    public Soin() {
        
    }
    
    public void setbaseSoin(int baseSoin) {
        this.baseSoin = baseSoin;
    }    
    
    public int getSoinEffet() {
        soinEffet = (int)(baseSoin * potionsize.getEffetAmeliore());
        return soinEffet;
    }
    
    /**
     * la potion est pris par quelqu'un
     * @param creature  tous les autres protagonistes
     */
    public void pickedBy(Creature creature) {
        if(!(creature instanceof Monstre) && (Statut == true)) {
            Personnage man = (Personnage) creature;
            man.underCure(this.getSoinEffet());
            Statut = false;
        }
    }
    
    public void affiche() {
        System.out.println("--------------------");
        System.out.println(this.type);
        System.out.println("-------------");
        System.out.println("BaseSoin: " + baseSoin);
        System.out.println("Potion Size: " + potionsize.name());
        System.out.println("Vrai Soin Effet: " + this.getSoinEffet());
    }
    
}
