/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.projet.objet;

import java.util.Random;

/**
 *
 * @author GUO
 */
public class Archer extends Personnage implements Combattant{

    private int nbFleches;

    /**
     *
     * @param nom Name of Archer
     * @param ptV Point vie
     * @param ptP Point Parad 
     * @param ptM Point Mana
     * @param pA Pourcentage Attack
     * @param pP Pourcentage defence
     * @param pM Pourcentage Magique
     * @param rM Pourcentage Resistance of Magic
     * @param dA Attack degree
     * @param dM Magic degree
     * @param distMax Max distance of attack
     * @param pos position of an archer
     * @param nbF nombre de fleches qu'il reste
     */
    public Archer(String nom, int ptV, int ptP,
            int ptM, int pA, int pP, int pM,
            int rM, int dA, int dM, int distMax,
            Point2D pos, int nbF) {
        super(nom, ptV, ptP, ptM, pA, pP, pM,
                rM, dA, dM, distMax, pos);
        nbFleches = nbF;
    }

    /**
     *
     * @param a objet qui est un type de Archer
     */
    public Archer(Archer a) {
        super(a.nom, a.ptVie, a.ptPar, a.ptMana, a.pourcentageAtt, a.pourcentagePar,
                a.pourcentageMag, a.pourcentageResistMag, a.degAtt, a.degMag,
                a.distAttMax, a.pos);
        this.nbFleches = a.nbFleches;

    }

    /**
     * sans parametres
     */
    public Archer() {
//        nbFleches = 0;
    }

    public int getNbFleches() {
        return nbFleches;
    }

    public void setNbFleches(int value) {
        nbFleches = value;
    }

    /**
     * Attack sur l'autre protagoniste, deux types de combattres: combattre
     * corps a corps et combattres a distance
     *
     * @param creature parametre entree et sa type est Creature pour compatible
     * avec les different type de protagonistes
     */
    public void combattre(Creature creature) {
        int degatSubit = 0;
        //corps a corps
        if ((int) (this.pos.distance(creature.pos)) == 1) {
            Random rand = new Random();
            if (rand.nextInt(100) > this.pourcentageAtt) {
                System.out.println("Attaque n'est pas reussi.");
            } else {
                if (rand.nextInt(100) > creature.pourcentagePar) {
                    degatSubit = this.degAtt;
                } else {
                    degatSubit = this.degAtt - creature.ptPar;
                }
            }
        } //a distance
        else if (this.pos.distance(creature.pos) < this.distAttMax) {
            Random rand = new Random();
            if (rand.nextInt(100) > this.pourcentageAtt) {
                System.out.println("Attack n'est pas reussi.");
            } else {
                degatSubit = this.degAtt;
            }
        }

        creature.underAttack(degatSubit);

    }

    public void affiche() {
        super.affiche();
        System.out.println("nbFleches: " + nbFleches);
    }
    
}
