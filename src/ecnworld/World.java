/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecnworld;

import java.util.*;
import org.centrale.projet.objet.*;

/**
 *
 * @author Exia
 */
public class World {
    public int range;
    public ArrayList<Point2D> caractereList; 
    public LinkedList<Creature> protoganisteEns;

    /**
     * 
     * @param range la taille du monde
     */
    public World(int range){
        this.range = range;
        this.caractereList = new ArrayList<>(); 
//        this.protoganisteEns = new ArrayList<>();
        this.protoganisteEns = new LinkedList<>();
    }
    public World() {
        range = 100;
        this.caractereList = new ArrayList<>();
        this.protoganisteEns = new LinkedList<>();
        
    }
    
    /**
     * creer un monde avec different type de vivants, 
     * leur position sont different.
     * @version 2.0
     */
    public void creeMondeAlea() {
        this.creeProtagonistesEns();
        Random x_aleatoire = new Random();
        Random y_aleatoire = new Random();
        x_aleatoire.setSeed(123);
        y_aleatoire.setSeed(546);
        caractereList.add(new Point2D(x_aleatoire.nextInt(range), y_aleatoire.nextInt(range)));
        int x_next;
        int y_next;
        Iterator<Creature> iter = protoganisteEns.iterator();
        
        while(true){
            x_next = x_aleatoire.nextInt(range);
            y_next = y_aleatoire.nextInt(range);
            int flag = 1;
            for (int i = 0; i < caractereList.size(); ++i) {
                double distance = Math.pow((x_next - caractereList.get(i).getX()), 2)
                        + Math.pow((y_next - caractereList.get(i).getY()), 2);

                if (distance > 25 || distance <= 0) {
                    flag = 0;
                }
            }
            if (flag == 1) {
                caractereList.add(new Point2D(x_next, y_next));
                if(!iter.hasNext()){
                    break;
                }
                iter.next().setPos(new Point2D(x_next, y_next));
                
            }
            
        }
    }
        
        
        
    
    public void TourDeJeu(){
        
    }
    
    
    
    public void creeProtagonistesEns () {
        Random nb;
        nb = new Random();
        int nbArcher = nb.nextInt(5);
        for (int i = 0; i < nbArcher; ++i) {
             protoganisteEns.add(new Archer());
            }
        int nbPaysan = nb.nextInt(5);
        for (int i = 0; i < nbPaysan; ++i) {
             protoganisteEns.add(new Paysan());
            }
        int nbLapin = nb.nextInt(5);
        for (int i = 0; i < nbLapin; ++i) {
             protoganisteEns.add(new Lapin());
            }
        int nbLoup = nb.nextInt(5);
        for (int i = 0; i < nbLoup; ++i) {
             protoganisteEns.add(new Loup());
            }
    }
    
    public void AffichageProtoganisteParIter(){
        Iterator<Creature> iter = protoganisteEns.iterator();
        while(iter.hasNext()){
            iter.next().affiche();
        }
    }
    
}
