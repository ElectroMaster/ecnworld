/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecnworld;
import org.centrale.projet.objet.*;
/**
 *
 * @author Exia
 */
public class TestSeance {

    public static void main(String[] args){
        long debut = System.currentTimeMillis();
        World myworld = new World();
        myworld.creeMondeAlea();
        myworld.AffichageProtoganisteParIter();
        long fin = System.currentTimeMillis();
        System.out.println("Temps ecoule en ms: "+ (fin - debut));
    }

}
